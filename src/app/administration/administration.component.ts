import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {Prop} from '../domain/prop';

@Component({
    selector: 'app-root',
    templateUrl: './administration.component.html',
    styleUrls: ['./administration.component.css'],
    providers: [HttpService]
})
export class AdministrationComponent implements OnInit {
    start: Date;
    end: Date;
    invalidRange  = false;
    isUpdateConfigSuccess = false;
    isUpdateConfigError = false;
    isSuccess = false;
    isError = false;
    loading = false;
    message: string;
    prop: Prop = new Prop();
    constructor(private httpService: HttpService, private router: Router) {
    }
    ngOnInit() {
        this.httpService.getPropData().then(prop => {
            this.prop = prop;
        });
    }

    onDelete() {
        if (this.start > this.end) {
            this.invalidRange = true;
        }

        this.httpService.deleteTx(this.start, this.end).subscribe(
            response => {
                this.loading = false;
                this.isSuccess = true;
            },
            response => {
                this.loading = false;
                this.isError = true;
            }
        );
    }

    onUpdateProp() {
        console.log(this.prop);
        this.httpService.updateConfig(this.prop).subscribe(
            response => {
                this.loading = false;
                this.isUpdateConfigSuccess = true;
            },
            response => {
                this.loading = false;
                this.isUpdateConfigError = true;
            }
        );
    }

}
