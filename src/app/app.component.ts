import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']

})
export class AppComponent {
    title = 'demoApp';
    formGroup: FormGroup;
    dateModel: Date = new Date();

    stringDateModel: string = new Date().toString();

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnInit() {
        this.formGroup = new FormGroup({
            activeEndDate: new FormControl(new Date(), {validators: [Validators.required, DateTimeValidator]})
        }, {updateOn: 'change'});
    }
}

export const DateTimeValidator = (fc: FormControl) => {
    const date = new Date(fc.value);
    const isValid = !isNaN(date.valueOf());
    return isValid ? null : {
        isValid: {
            valid: false
        }
    };
};
