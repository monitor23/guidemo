import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {AppComponent} from './app.component';
import {MtComponent} from './mt/mt.component';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {HeaderComponent} from './header/header.component';
import {appRoutingModule} from './app.routing';
import {MessagesModule} from 'primeng/messages';
import {PanelModule} from 'primeng/panel';
import {CardModule} from 'primeng/card';
import {ChartModule} from 'primeng/chart';
import {MenubarModule} from 'primeng/menubar';
import {TabBarComponent} from './tab-bar/tab-bar.component';
import {LogComponent} from './log/log.component';
import {MqComponent} from './operation/mq.component';
import {ChartComponent} from './chart/chart.component';
import {TabMenuModule} from 'primeng/tabmenu';
import {FooterComponent} from './footer/footer.component';
import {HttpService} from './service/http.service';
import {AdministrationComponent} from './administration/administration.component';
import {CalendarModule} from 'primeng/calendar';
import {MqViewComponent} from './mqView/mqView.component';
import {InputNumberModule} from 'primeng/inputnumber';
import {BicDetailsComponent} from './bicDetails/bicDetails.component';
import {ReleaseComponent} from './release/release.component';

@NgModule({
    declarations: [
        AppComponent,
        MtComponent,
        LogComponent,
        MqComponent,
        ChartComponent,
        AdministrationComponent,
        BicDetailsComponent,
        HeaderComponent,
        MqViewComponent,
        FooterComponent,
        TabBarComponent,
        ReleaseComponent
    ],
    imports: [
        appRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        TableModule,
        HttpClientModule,
        InputTextModule,
        DialogModule,
        ButtonModule,
        DropdownModule,
        MultiSelectModule,
        PanelModule,
        MessagesModule,
        FormsModule,
        CardModule,
        ChartModule,
        MenubarModule,
        TabMenuModule,
        CalendarModule,
        InputNumberModule
    ],
    providers: [HttpService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
