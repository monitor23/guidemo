import {RouterModule, Routes} from '@angular/router';
import {MtComponent} from './mt/mt.component';
import {LogComponent} from './log/log.component';
import {ChartComponent} from './chart/chart.component';
import {MqComponent} from './operation/mq.component';
import {AdministrationComponent} from './administration/administration.component';
import {MqViewComponent} from './mqView/mqView.component';
import {BicDetailsComponent} from './bicDetails/bicDetails.component';
import {ReleaseComponent} from './release/release.component';

const routes: Routes = [
    {path: 'mt', component: MtComponent},
    {path: 'logs', component: LogComponent},
    {path: 'chart', component: ChartComponent},
    {path: 'operation', component: MqComponent},
    {path: 'mqView', component: MqViewComponent},
    {path: 'admin', component: AdministrationComponent},
    {path: 'bic', component: BicDetailsComponent},
    {path: 'release', component: ReleaseComponent},
    { path: '',   redirectTo: '/mt', pathMatch: 'full' },

];

export const appRoutingModule = RouterModule.forRoot(routes);
