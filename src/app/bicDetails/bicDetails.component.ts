import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {Prop} from '../domain/prop';

@Component({
    selector: 'app-root',
    templateUrl: './bicDetails.component.html',
    styleUrls: ['./bicDetails.component.css'],
    providers: [HttpService]
})
export class BicDetailsComponent implements OnInit {
    bic: string;
    bicResponse: string;
    isSuccess: boolean;
    isError: boolean;
    constructor(private httpService: HttpService) {
    }
    ngOnInit() {
    }

    showBicProp() {
        this.httpService.showBicDetails(this.bic).subscribe(
            response => {
                this.bicResponse = JSON.stringify(response, null, '\t');
                this.isSuccess = true;
            },
            response => {
                this.isError = true;
            }
        );
    }
}
