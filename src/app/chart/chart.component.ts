import {Component, OnInit} from '@angular/core';
import {Doughnut} from '../domain/doughnut';
import {Bar} from '../domain/bar';
import {HttpService} from '../service/http.service';

export class PrimeDoughnut implements Doughnut {
    constructor(public pdeCount?, public nonPdeCount?) {
    }
}


@Component({
    selector: 'app-root',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.css'],
    providers: [HttpService]
})
export class ChartComponent implements OnInit {

    barChartData: any;

    doughnutChartData: any;

    msgs: any[];

    doughnut: Doughnut = new PrimeDoughnut();
    bar: Bar = new Bar();

    constructor(private httpService: HttpService) {

    }

    ngOnInit() {

        this.httpService.getBarData().then(bar => {
            this.bar = bar;
            this.barChartData = {
                labels: this.bar.labels,
                datasets: [
                    {
                        label: 'Valid transactions',
                        backgroundColor: '#1b4814',
                        borderColor: '#1b4814',
                        data: this.bar.valid
                    },
                    {
                        label: 'Not validated transactions',
                        backgroundColor: '#e8ea4e',
                        borderColor: '#e8ea4e',
                        data: this.bar.notValidated
                    },
                    {
                        label: 'Invalid BIC transactions',
                        backgroundColor: '#b17923',
                        borderColor: '#b17923',
                        data: this.bar.invalidBic
                    },
                    {
                        label: 'Invalid format transactions',
                        backgroundColor: '#d4301c',
                        borderColor: '#d4301c',
                        data: this.bar.invalidFormat
                    }
                ]
            };
        });
        this.httpService.getDoughnutData().then(doughnut => {
            this.doughnut = doughnut;
            this.doughnutChartData = {
                labels: ['PDETX', 'NONPDETX'],
                datasets: [
                    {
                        data: [this.doughnut.pdeCount, this.doughnut.nonPdeCount],
                        backgroundColor: [
                            '#ffb6c1',
                            '#707d90',
                            '#FFCE56'
                        ],
                        hoverBackgroundColor: [
                            '#ffb6c1',
                            '#708090',
                            '#FFCE56'
                        ]
                    }]
            };
        });
        this.msgs = [];


    }
}
