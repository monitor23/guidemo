export interface Mq {
    name?;
    curdepth?;
    maxdepth?;
    inputc?;
    outputc?;
    maxmsgl?;
    defpsist?;
    stats?;
    enq?;
    deq;
    time?;
    clase?;
}