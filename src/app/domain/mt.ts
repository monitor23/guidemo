export interface Mt {
    mtFileName?;
    mtFileContent?;
    mtCreatedDate?;
    pacsFileName?;
    pacsFileContent?;
    pacsCreatedDate?;
    pdeFlag?;
    pacsStatus?;
    rejectedReason?;
}
