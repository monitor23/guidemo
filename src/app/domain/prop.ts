export class Prop {
    inputFolder?;
    outputFolder?;
    mqChannel?;
    mqHost?;
    mqPort?;
    mqName?;
    inQueue?;
    retriesNo?;
}
