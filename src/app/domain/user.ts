export interface User {
    id?;
    username?;
    lastName?;
    firstName?;
    password?;
    email?;
    role?;
    createDate?;
    authdata?;
}
