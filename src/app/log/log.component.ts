import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-root',
    templateUrl: './log.component.html',
    styleUrls: ['./log.component.css'],
   // providers: [LogService]
})
export class LogComponent implements OnInit {
    content = '';
    constructor(private http: HttpClient) {
    }

    ngOnInit() {
        this.http.get('http://172.31.31.93:8080/actuator/logfile',
            {responseType: 'text'}).subscribe((content: string) => this.content = content);
    }

}
