import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {Mq} from '../domain/mq';

export class PrimeMq implements Mq {
    clase;
    curdepth;
    defpsist;
    deq;
    enq;
    inputc;
    maxdepth;
    maxmsgl;
    name;
    outputc;
    stats;
    time;
}

@Component({
    selector: 'app-root',
    templateUrl: './mqView.component.html',
    styleUrls: ['./mqView.component.css'],
    providers: [HttpService]
})
export class MqViewComponent implements OnInit {


    mq: Mq = new PrimeMq();
    mqs: Mq[];
    cols: any[];

    constructor(private httpService: HttpService, private router: Router) {
    }

    ngOnInit() {
        // similar with charts model
        this.httpService.getMqDetails().then(mqs => {
            this.mqs = mqs;
        });
        this.cols = [
            {field: 'name', header: 'Name'},
            {field: 'curdepth', header: 'CurDepth'},
            {field: 'maxdepth', header: 'MaxDepth'},
            {field: 'maxmsgl', header: 'Max Message Length'},
            {field: 'inputc', header: 'Inputc'},
            {field: 'outputc', header: 'Outpuc'},
            {field: 'defpsist', header: 'Persistent'}
        ];
    }


}
