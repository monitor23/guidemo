import {Component, OnInit} from '@angular/core';
import {Mt} from '../domain/mt';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';

export class PrimeMt implements Mt {
    constructor(public mtFileName?, public mtFileContent?, public mtCreatedDate?, public pacsFileName?,
                public pacsFileContent?, public pacsCreatedDate?, public pdeFlag?, public pacsStatus?, public rejectedReason?) {
    }
}

@Component({
    selector: 'app-root',
    templateUrl: './mt.component.html',
    styleUrls: ['./mt.component.css'],
    providers: [HttpService]
})
export class MtComponent implements OnInit {
    message: string;

    file: File = null;
    displayDialog: boolean;

    mt: Mt = new PrimeMt();

    selectedMt: Mt;

    newMt: boolean;
    mts: Mt[];
    cols: any[];

    constructor(private httpService: HttpService, private router: Router) {
    }

    ngOnInit() {
        // similar with charts model
        this.httpService.getAll().then(mts => {
            this.mts = mts;
        });
        this.cols = [
            {field: 'mtFileName', header: 'MT'},
            {field: 'mtCreatedDate', header: 'MT Created Date'},
            {field: 'pacsFileName', header: 'MX'},
            {field: 'pacsCreatedDate', header: 'MX Created Date'},
            {field: 'pacsStatus', header: 'MX Status'},
            {field: 'rejectedReason', header: 'MX Rejected Reason'},
            {field: 'pdeFlag', header: 'PDE'}
        ];
    }

    onRowSelect(event) {
        this.newMt = false;
        this.mt = {...event.data};
        this.displayDialog = true;
    }

    refreshTransactionsPage() {
        window.location.reload();
    }
}
