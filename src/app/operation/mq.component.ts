import {Component, OnInit} from '@angular/core';
import {delay} from 'rxjs/operators';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';

@Component({
    selector: 'app-root',
    templateUrl: './mq.component.html',
    styleUrls: ['./mq.component.css'],
    providers: [HttpService]
})
export class MqComponent implements OnInit {
    message: string;
    file: File = null;
    filename: any;
    filenameBatch: any;
    isSucces = false;
    isSuccesQ = false;
    isSuccesBatch = false;
    isErrorQ = false;
    isErrorBatch = false;
    loading  = false; // Flag variable
    isError  = false; // Flag variable
    loadingBatch  = false; // Flag variable
    displayDuplicate  = false; // Flag variable
    isInvalid = false; // Flag variable
    isInvalidBatch = false; // Flag variable
    displayDialogUpload: boolean;
    constructor(private httpService: HttpService, private router: Router) {
    }
    ngOnInit() {
    }


    onUploadToQ() {
        this.displayDuplicate = false;
        this.loading = !this.loading;
        this.httpService.uploadMq(this.file).subscribe(
            response => {
                this.loading = false;
                this.isSuccesQ = true;
            },
            response => {
                this.loading = false;
                this.isErrorQ = true;
            }
        );
    }
    onChange(event) {
        this.file = event.target.files[0];
        this.filename = this.file.name;
        this.isInvalid = false;
        this.loading = false;
        this.isError = false;
        this.isSuccesQ = false;
        this.isErrorQ = false;
        this.displayDuplicate = false;
        this.isSucces = false;
        this.displayDialogUpload = true;
    }

    onBatchUpload(event) {
        this.file = event.target.files[0];
        this.filenameBatch = this.file.name;
        this.displayDialogUpload = true;
        if (this.file.type === 'text/plain') {
            this.loadingBatch = !this.loadingBatch;
        this.httpService.batchUpload(this.file).subscribe(
            response => {
                this.loadingBatch = false;
                this.isSuccesBatch = true;
                setTimeout(() => {
                    this.router.navigate(['/mt']); } , 5000
                );
            },
            response => {
                this.loadingBatch = false;
                this.isErrorBatch = true;
            }
        );
        } else {
            this.isInvalidBatch = true;
            this.loadingBatch = false;
            this.isSuccesBatch = false;
        }
    }

    onUpload() {

        this.isSuccesQ = false;
        this.isErrorQ = false;
        this.displayDuplicate = false;
        if (this.file.type === 'text/plain') {
            this.loading = !this.loading;
            this.isInvalid = false;
            this.httpService.upload(this.file).subscribe(
                response => {
                    this.loading = false;
                    this.message = response.message;
                    this.isSucces = true;
                    this.isError = false;
                    delay(9000);
                    this.router.navigate(['/mt']);
                },
                response => {
                    console.log('PUT call in error', response);
                    this.loading = false;
                    this.isError = true;
                    this.isSucces = false;
                    this.message = response.error.message;
                    if (this.message === 'File content already exists') {
                        this.displayDuplicate = true;
                    }
                }
            );
        } else {
            this.isInvalid = true;
            this.loading = false;
            this.isError = false;
            this.isSucces = false;
        }
    }
    onUploadDuplicate() {

        this.isSuccesQ = false;
        this.isErrorQ = false;
        this.displayDuplicate = false;
        if (this.file.type === 'text/plain') {
            this.loading = !this.loading;
            this.isInvalid = false;
            this.isError = false;
            this.isSucces = false;
            this.httpService.uploadDuplicate(this.file).subscribe(
                response => {
                    this.loading = false;
                    this.message = response.message;
                    this.isSucces = true;
                    this.isError = false;
                    delay(9000);
                    this.router.navigate(['/mt']);
                },
                response => {
                    console.log('PUT call in error', response);
                    this.loading = false;
                    this.isError = true;
                    this.isSucces = false;
                    this.message = response.error.message;
                }
            );
        } else {
            this.isInvalid = true;
            this.loading = false;
            this.isError = false;
            this.isSucces = false;
        }
    }
}
