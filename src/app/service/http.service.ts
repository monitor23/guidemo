import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Mt} from '../domain/mt';
import {Observable} from 'rxjs';
import {Doughnut} from '../domain/doughnut';
import {Bar} from '../domain/bar';
import {Prop} from '../domain/prop';
import {formatDate} from '@angular/common';
import {Mq} from '../domain/mq';

@Injectable()
export class HttpService {

    url = 'http://172.31.31.93:8080/';
    producerUrl = 'http://localhost:8383/';

    constructor(private http: HttpClient) {}

    getAll() {
        return this.http.get<Mt[]>(this.url + 'all')
            .toPromise()
            .then(data => data);
    }

    getMqDetails() {
        return this.http.get<Mq[]>(this.url + 'viewMQ')
            .toPromise()
            .then(data => data);
    }

    upload(file): Observable<any> {

        const formData = new FormData();

        formData.append('file', file, file.name);

        // @ts-ignore
        return this.http.post(this.url + 'upload', formData);
    }
    uploadDuplicate(file): Observable<any> {

        const formData = new FormData();

        formData.append('file', file, file.name);

        // @ts-ignore
        return this.http.post(this.url + 'uploadDuplicate', formData);
    }

    uploadMq(file): Observable<any> {

        const formData = new FormData();


        formData.append('file', file, file.name);

        return this.http.post(this.url + 'uploadMQ', formData);
    }

    uploadDummy(): Observable<any> {
        return this.http.get(this.url + 'uploadMQDummy');
    }
    getDoughnutData() {
        return this.http.get<Doughnut>(this.url + 'pdeNonPde')
            .toPromise()
            .then(data => data);
    }
    getBarData() {
        return this.http.get<Bar>(this.url + 'txByDay')
            .toPromise()
            .then(data => data);
    }

    deleteTx(start: Date, end: Date): Observable<any> {
        let params = new HttpParams();

        const startD =  formatDate(start, 	'yyyy-MM-dd', 'en-US');
        const endD =  formatDate(end, 'yyyy-MM-dd', 'en-US');
        console.log(startD);
        console.log(endD);
        params = params.append('start', startD );
        params = params.append('end', endD);
        // return this.http.get<string>(this.url + 'deleteTx?start=' + start + '&end=' + end )
        return this.http.get<any>(this.url + 'deleteTx', {params: params} );
    }

    getPropData() {
        return this.http.get<Prop>(this.url + 'getConfig')
            .toPromise()
            .then(data => data);
    }

    updateConfig(prop: Prop): Observable<Prop> {
        return this.http.post(this.url + 'updateConfig', prop);
    }

    showBicDetails(bic: string) {
        return this.http.get<any>(this.url + 'bicDetails/' + bic);
    }

    batchUpload(file): Observable<any> {

        const formData = new FormData();

        formData.append('file', file, file.name);

        // @ts-ignore
        return this.http.post(this.producerUrl + 'batchUpload', formData);
    }
}

