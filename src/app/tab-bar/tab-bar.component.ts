import {MenuItem} from 'primeng/api';
import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-tab-menu',
    templateUrl: './tab-menu.component.html'})
export class TabBarComponent implements OnInit {

    items: MenuItem[];

    ngOnInit() {
        this.items = [
            {label: 'Transactions', icon: 'fa fa-fw fa-book', routerLink: ['/mt']},
            {label: 'Operations', icon: 'fa fa-fw fa-book', routerLink: ['/operation']},
            {label: 'Statistics', icon: 'fa fa-fw fa-book', routerLink: ['/chart']},
            {label: 'Administration', icon: 'fa fa-fw fa-book', routerLink: ['/admin']},
            {label: 'BIC Details', icon: 'fa fa-fw fa-book', routerLink: ['/bic']},
            {label: 'MQ Details', icon: 'fa fa-fw fa-book', routerLink: ['/mqView']},
            {label: 'Release notes', icon: 'fa fa-fw fa-book', routerLink: ['/release']},
            {label: 'Logs', icon: 'fa fa-fw fa-book', routerLink: ['/logs']},
        ];
    }
}
